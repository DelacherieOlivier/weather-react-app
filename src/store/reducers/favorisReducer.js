import {createSlice} from '@reduxjs/toolkit';

export const favorisSlice = createSlice({
  name: 'favoris',
  initialState: {
      listOfFavoris: [],
  },
  reducers: {
    addFavoris: (state, action) => {
      if (state.listOfFavoris.find(favoris => favoris.id === action.payload.id)) {
        alert("deja dans les favoris");
      }else {
        state.listOfFavoris.push(action.payload);
      }
    },
    removeFavoris: (state, action) => {
      state.listOfFavoris = state.listOfFavoris.filter(favoris => favoris.id !== action.payload.id);
    },
  },
});

export const {addFavoris,removeFavoris} = favorisSlice.actions;

export default favorisSlice.reducer
