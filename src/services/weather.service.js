import LocationHelper from "../helpers/location.helper";
import WeatherRepository from "../repository/weather.repository";
import WeatherFactory from "../factory/weather.factory";

const WeatherService = {
    async getWeatherByLocation(){
        const location = await LocationHelper.getCurrentLocation()
        const weather = await this.getWeatherByCoords(location.coords.latitude, location.coords.longitude);
        return weather;
    },
    async getWeatherByCoords(lat,lon){
        const weatherRawData = await WeatherRepository.getMeteoByCoords(lat, lon);
        const weather = WeatherFactory.formatRawWeatherToWeather(weatherRawData);
        return weather;
    },
    async getWeatherForecastByLocation(){
        const location = await LocationHelper.getCurrentLocation()
        const weatherForecast = await this.getWeatherForecastByCoords(location.coords.latitude, location.coords.longitude);
        return weatherForecast;
    },
    async getWeatherForecastByCoords(lat,lon){
        const weatherForecastRawData = await WeatherRepository.getMeteoByCoordsForcast(lat, lon);
        const weatherForecast = WeatherFactory.formatRawWeatherForecastToWeatherForecast(weatherForecastRawData);
        return weatherForecast;
    },
}

export default WeatherService;