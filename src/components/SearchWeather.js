import { useState } from 'react';
import weatherRepository from '../repository/weather.repository';
import WeatherCardSearch from "./WeatherCardSearch";

function SearchWeather () {

    const [input, setInput] = useState('');
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        weatherRepository.getWeather(input).then(data => {
            setData(data);
        }).catch(error => {
            setError(error);
        });
    }

    return (
        <div id="AppMeteo">
            <main>
                <form className="custom-flex" onSubmit={handleSubmit}>
                    <input type="text" placeholder="Entrez une ville" value={input} onChange={ ({ target }) => setInput(target.value) } />
                    <button type="submit">Rechercher</button>
                </form>
                { data != null ? <WeatherCardSearch weatherData={data}/> : null}
            </main>
        </div>
    );
}

export default SearchWeather;