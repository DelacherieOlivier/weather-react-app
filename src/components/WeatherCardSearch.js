import React from 'react';
import FormFavoris from "./FormFavoris";

export default function WeatherCardSearch({weatherData}) {


    return (
        <div className="main">
            <h2 className="section_title">{weatherData.name}</h2>
            <div className="card">
                <div className="custom-flex">
                    <div className="custom-flex_column">
                        <p className="temp">Temperature : {Math.round(weatherData.main.temp)} &deg;C</p>
                        <p className="description">Temp : {weatherData.weather[0].main}</p>
                        <p className="description">Humidité : {weatherData.main.humidity}</p>
                    </div>
                    <div className="custom-flex_column">
                        <FormFavoris  weatherData={weatherData}/>
                    </div>
                </div>
            </div>
        </div>

    );
}