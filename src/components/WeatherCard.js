import React from 'react';

import {
    faCloud,
    faBolt,
    faCloudRain,
    faCloudShowersHeavy,
    faSnowflake,
    faSun,
    faSmog,
} from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const WeatherIcon = styled.div`color: whitesmoke;`;

export default function WeatherCard({weatherData}) {
    let weatherIcon = null;

    if (weatherData.main === 'Thunderstorm') {
        weatherIcon = <FontAwesomeIcon icon={faBolt} />;
    } else if (weatherData.main === 'Drizzle') {
        weatherIcon = <FontAwesomeIcon icon={faCloudRain} />;
    } else if (weatherData.main === 'Rain') {
        weatherIcon = <FontAwesomeIcon icon={faCloudShowersHeavy} />;
    } else if (weatherData.main === 'Snow') {
        weatherIcon = <FontAwesomeIcon icon={faSnowflake} />;
    } else if (weatherData.main === 'Clear') {
        weatherIcon = <FontAwesomeIcon icon={faSun} />;
    } else if (weatherData.main === 'Clouds') {
        weatherIcon = <FontAwesomeIcon icon={faCloud} />;
    } else {
        weatherIcon = <FontAwesomeIcon icon={faSmog} />;
    }

    return (
        <div className="main">
            <h2 className="section_title">{weatherData.name}</h2>
            <div className="card">
                <div className="card-time">Le {weatherData.date}</div>
                <div className="custom-flex">
                    <div className="custom-flex_column">
                        <p className="temp">Temprature : {weatherData.temp} &deg;C</p>
                        <p className="description">Temp : {weatherData.main}</p>
                        <p className="description">Humidité : {weatherData.humidity}</p>
                    </div>
                    <div className="custom-flex_column">
                        <WeatherIcon>{weatherIcon}</WeatherIcon>
                    </div>
                </div>

            </div>
        </div>
    )
}