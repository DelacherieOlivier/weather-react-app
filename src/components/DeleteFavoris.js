import React, { Component } from 'react';
import { connect } from 'react-redux';
import {removeFavoris} from "../store/reducers/favorisReducer";

class Delete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listOfFavoris: [],
            weatherData:null,
        }
    }

    submitForm= (e) => {
        this.props.removeFavoris(this.props.weatherData);
        this.setState({
            weatherData:null,
        })
    }

    render() {
        return (
            <button type="button" className="btn-delete" onClick={(e)=> this.submitForm(e)}>remove favoris</button>
        );
    }

}

const mapDispatchToProps = dispatch => {
    return {
        removeFavoris: (favoris) => {
            dispatch(removeFavoris(favoris));
        },
    }
};

const mapStateToProps = state => {
    return {
        listOfFavoris: state.favoris.listOfFavoris
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Delete);

