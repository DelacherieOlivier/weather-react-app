import React, { Component } from 'react';
import { connect } from 'react-redux';
import {addFavoris} from "../store/reducers/favorisReducer";

class FormFavoris extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listOfFavoris: [],
            weatherData:null,
        }
    }

    submitForm = () =>{
        this.props.addFavoris(this.props.weatherData);
        this.setState({
            weatherData:null,
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.listOfFavoris !== this.props.listOfFavoris) {
            this.setState({listOfFavoris: this.props.listOfFavoris})
        }
    }

    render() {
        return (
            <button type="button" className="btn" onClick={(e)=> this.submitForm(e)}>add favoris</button>
        );
    }

}

const mapDispatchToProps = dispatch => {
    return {
        addFavoris: (favoris) => {
            dispatch(addFavoris(favoris));
        },
    }
};

const mapStateToProps = state => {
    return {
        listOfFavoris: state.favoris.listOfFavoris
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormFavoris);

