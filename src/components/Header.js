import {Link} from 'react-router-dom';

export default function Header(){
    return (
        <header className="header">
            <div className="container">
                <nav className="navigation" role="navigation">
                    <div className="menue">
                        <Link to="/home">Home</Link>
                        <Link to="/search">Recherche</Link>
                        <Link to="/favoris">Favoris</Link>
                    </div>
                </nav>
            </div>
        </header>
    );
}