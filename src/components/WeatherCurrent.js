import React, { useEffect, useState } from "react";
import WeatherCard from './WeatherCard';
import WeatherService from "../services/weather.service";

export default function WeatherCurrent() {

    const [weather, setWeather] = useState(null);

    useEffect(async () => {
        const weather = await WeatherService.getWeatherByLocation();
        setWeather(weather);
    }, []);

    return (
        <div className="App">
            { weather != null ? <WeatherCard weatherData={weather}/> : null}
        </div>
    );
}
