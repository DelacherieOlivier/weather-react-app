import React, {useEffect, useState} from "react";
import WeatherCard from './WeatherCard';
import weatherRepository from '../repository/weather.repository';
import WeatherService from "../services/weather.service";

export default function WeatherForcast() {

    const [weathers, setWeathers] = useState(null);

    useEffect(async () => {
        const weathers = await WeatherService.getWeatherForecastByLocation();
        setWeathers(weathers);
    }, []);

    return (
        <div className="weather-forcast">
            {weathers && weathers.map((forecast, index) => {
                return <WeatherCard key={index} weatherData={forecast}/>
            })}
        </div>
    );
}