import WeatherHelper from "../helpers/weather.helper";

const WeatherFactory = {
    formatRawWeatherToWeather(rawWeather) {
        return {
            name: rawWeather.name,
            main: rawWeather.weather[0].main,
            date: WeatherHelper.convertDateToLocaleDate(rawWeather.dt_txt),
            temp: Math.round(rawWeather.main.temp),
            humidity: rawWeather.main.humidity,
        }
    },
    formatRawWeatherForecastToWeatherForecast(rawWeatherForecast) {
        return rawWeatherForecast.list.map((rawWeather) => {
            return this.formatRawWeatherToWeather(rawWeather);
        }).slice(0, 8);
    }
}

export default WeatherFactory;