import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faBolt,
    faCloud,
    faCloudRain,
    faCloudShowersHeavy,
    faSmog,
    faSnowflake,
    faSun
} from "@fortawesome/free-solid-svg-icons";
import React from "react";

const WeatherHelper = {
    convertDateToLocaleDate(date) {
        let newDate = new Date(date);
        return newDate.toLocaleString('fr-FR', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        });
    },
    getWeatherIconByMain(main) {
        let weatherIcon = null;

        if (main === 'Thunderstorm') {
            weatherIcon = <FontAwesomeIcon icon={faBolt}/>;
        } else if (main === 'Drizzle') {
            weatherIcon = <FontAwesomeIcon icon={faCloudRain}/>;
        } else if (main === 'Rain') {
            weatherIcon = <FontAwesomeIcon icon={faCloudShowersHeavy}/>;
        } else if (main === 'Snow') {
            weatherIcon = <FontAwesomeIcon icon={faSnowflake}/>;
        } else if (main === 'Clear') {
            weatherIcon = <FontAwesomeIcon icon={faSun}/>;
        } else if (main === 'Clouds') {
            weatherIcon = <FontAwesomeIcon icon={faCloud}/>;
        } else {
            weatherIcon = <FontAwesomeIcon icon={faSmog}/>;
        }

        return weatherIcon;
    }
}

export default WeatherHelper;