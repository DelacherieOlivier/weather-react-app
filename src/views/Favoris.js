import React, {Component} from 'react';
import {connect} from "react-redux";
import Delete from "../components/DeleteFavoris";

class Favoris extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listOfFavoris: []
    };
  }
  componentDidMount() {
    this.setState({listOfFavoris: this.props.listOfFavoris});
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.listOfFavoris !== this.props.listOfFavoris) {
      this.setState({listOfFavoris: this.props.listOfFavoris})
    }
  }

  render() {
    return (
      <main className="container">
        <div className="favoris">
          <h1 className="section_title">Liste des favoris</h1>
          {this.state.listOfFavoris.length > 0 && this.state.listOfFavoris.map((weatherData, index) => {
            return (
                <div className="main" key={index}>
                  <h2 className="section_title">{weatherData.name}</h2>
                  <div className="card">
                    <div className="custom-flex">
                      <div className="custom-flex_column">
                        <p className="temp">Temperature : {Math.round(weatherData.main.temp)} &deg;C</p>
                        <p className="description">Temp : {weatherData.weather[0].main}</p>
                        <p className="description">Humidité : {weatherData.main.humidity}</p>
                      </div>
                      <div className="custom-flex_column">
                        <Delete  weatherData={weatherData}/>
                      </div>
                    </div>
                  </div>
                </div>
            );
          })}
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    listOfFavoris: state.favoris.listOfFavoris
  };
};

export default connect(mapStateToProps)(Favoris);