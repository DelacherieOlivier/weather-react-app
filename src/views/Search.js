import React, {Component} from 'react';
import SearchWeather from '../components/SearchWeather';


class Search extends Component {
  render() {
    return (
      <main className="container" id="search">
        <h1 className="section_title">Rechercher une ville</h1>
        <SearchWeather />
      </main>
    );
  }
}

export default Search;