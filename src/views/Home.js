import React, {Component} from 'react';
import WeatherCurrent from "../components/WeatherCurrent";
import WeatherForcast from "../components/WeatherForcast";


class Home extends Component {
  render() {
    return (
      <main className="container">
          <div className="weatherCurrent">
              <WeatherCurrent />
          </div>
          <div className="weatherforcast">
              <h2 className="section_title">prévisition 24h</h2>
              <WeatherForcast />
          </div>
      </main>
    );
  }
}

export default Home;